executable = "C:\\LaserCut53\\LaserCut53.exe";
logpath = "log.csv";

function string:split( inSplitPattern, outResults )

   if not outResults then
      outResults = { }
   end
   local theStart = 1
   local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   while theSplitStart do
      table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
      theStart = theSplitEnd + 1
      theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   end
   table.insert( outResults, string.sub( self, theStart ) )
   return outResults
end
    
do local object = fltk:Fl_Double_Window(387, 151, "Laser Log");
  window = object;
  
  user = fltk:Fl_Input_Choice(90, 10, 280, 35, "User:");
  user:labelsize(20);
  user:textsize(20);
  user:when(fltk.FL_WHEN_CHANGED);
  
  material = fltk:Fl_Input_Choice(90, 57, 280, 35, "Material:");
  material:labelsize(20);
  material:textsize(20);
  material:deactivate();
  material:when(fltk.FL_WHEN_CHANGED);
  
  ok = fltk:Fl_Return_Button(275, 105, 95, 35, "Start");
  ok:deactivate();
end

file, err = io.open(logpath,"r");

if err == nil then
  for line in file:lines() do
    lines = line:split(',');
    print (line);
    user:add(lines[3]);
    material:add(lines[4]);
  end
  
  file:close();
end

file, err = io.open(logpath,"a");

user:callback(
  function(user)
    material:activate();
  end
)

material:callback(
  function(material)
    ok:activate();
  end
)

ok:callback(
  function(ok)
    local out = os.date("%Y/%m/%d,%H:%M")..','..user:value():upper()..','..material:value():upper().."\n";
    file:write(out);
    file:close();
    os.execute(executable);
    window:hide();
  end
)

window:show();
Fl:run();